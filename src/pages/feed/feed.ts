import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

@Component({
	selector: 'page-feed',
	templateUrl: 'feed.html'
})  
export class FeedPage {
	public feedName : string;
    public posts : any[];

	constructor(public viewCtrl: ViewController, public navParams: NavParams, public http: Http) {
        this.feedName = navParams.get('title');
        let rssUrl = navParams.get('URL');
        
        http.get('https://api.rss2json.com/v1/api.json?rss_url=' + encodeURI(rssUrl))
            .subscribe(res => {
                console.log(res.json().items);
                this.posts = res.json().items;
            });
	}
}