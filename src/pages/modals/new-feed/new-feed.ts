import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms'

@Component({
	selector: 'modal-new-feed',
	templateUrl: 'new-feed.html'
})  
export class NewFeedModal {
	private feed : FormGroup;
		
	constructor(public viewCtrl: ViewController, private formBuilder: FormBuilder) {
		this.feed = this.formBuilder.group({
			title:['', Validators.required],
			URL: ['', Validators.required]
		});
	}

	closeModal() {
		this.viewCtrl.dismiss(this.feed);
	}
}