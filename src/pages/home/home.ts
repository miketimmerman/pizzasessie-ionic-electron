import { Component } from '@angular/core';
import { NavController, ToastController, ModalController } from 'ionic-angular';
import { NewFeedModal } from '../modals/new-feed/new-feed';
import { FeedPage } from '../feed/feed';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public feeds : any[];
  
  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public toastCtrl: ToastController) {
    let lsFeeds = localStorage.feeds;

    // examples
    // CSS tricks - https://css-tricks.com/feed/
    // David Walsh - https://davidwalsh.name/feed
    
    this.feeds = [];

    if(lsFeeds){
      this.feeds = JSON.parse(lsFeeds);
    }
  }

  /**
   * Show success message when a new feed is added.
   */
  public showSuccessMessage(){
    let toast = this.toastCtrl.create({
      message: 'New RSS feed was added successfully',
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  /**
   * Create and show the modal for adding new RSS feeds.
   */
  public presentNewFeedModal(){
    let newFeedModal = this.modalCtrl.create(NewFeedModal);

    newFeedModal.onDidDismiss(data => {
      console.log(data);

      if(data.valid){
        this.addFeed(data.value);
      }
    });

    newFeedModal.present();
  }

  /**
   * Add a new RSS feed to feeds array and store it in localStorage.
   */
  public addFeed(source : object){
    this.feeds.push(source);
    localStorage.feeds = JSON.stringify(this.feeds);
    // Woohoo new feed adde. Show message.
    this.showSuccessMessage();
  }

  
  public goToFeed(feed : object){
    this.navCtrl.push(FeedPage, feed);
  }
}
