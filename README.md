# Pizza sessie: ionic + electron
## Setup ionic 

Please make sure you have nodejs installed: https://nodejs.org/en/ 

Install ionic and cordova
 	
~~~sh
> npm install -g cordova ionic 
~~~
 	
Create a standard project; sidemenu, tabs or blank

~~~sh
> ionic start pizzaSessie blank 
~~~

Go into project folder

~~~sh	
> cd pizzaSessie/ 
~~~

Open in browser

~~~sh	
> ionic serve 
~~~

Open in iOs emulator

~~~sh
> ionic run ios
~~~

## Setup electron

Create file 

~~~sh
> touch electron.js  
~~~

Fill with 

~~~javascript
"use strict";    
const {app, BrowserWindow} = require('electron') 
const path = require('path') 
const url = require('url') 
  
let mainWindow 
  
function createWindow () { 
    mainWindow = new BrowserWindow({width: 800, height: 600}) 
  
    mainWindow.loadURL(url.format({ 
        pathname: path.join(__dirname, 'www/index.html'), 
        protocol: 'file:', 
        slashes: true 
    })) 
  
    mainWindow.on('closed', function () { 
        mainWindow = null 
    }) 
} 
 
app.on('ready', createWindow) 
  
app.on('window-all-closed', function () { 
    if (process.platform !== 'darwin') { 
        app.quit() 
    } 
}) 
app.on('activate', function () { 
    if (mainWindow === null) { 
        createWindow() 
    } 
}) 
~~~
	 
Install electron dependency

~~~sh
> npm install electron --save-dev 
~~~

Edit package.json 

~~~json
{
  	...
  	"main": "electron.js",
	...
  	"scripts": {
   		"electron": "electron ."
  	}
}
~~~
	

Now lets start the electron application

~~~sh	
> npm run electron
~~~ 


## Package application

Lets install another npm package.

~~~sh	
> npm install electron-packager --save-dev
~~~ 

Add tasks

~~~js	
...
"scripts" : {
	...
	"package-mac": "electron-packager . --overwrite --platform=darwin --arch=x64 --icon=assets/icons/mac/icon.icns --prune=true --out=release-builds",
	"package-win": "electron-packager . --overwrite --asar=true --platform=win32 --arch=ia32 --icon=assets/icons/win/icon.ico --prune=true --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName=\"Electron Tutorial App\"",
	"package-linux" : "electron-packager . --overwrite --platform=linux --arch=x64 --icon=assets/icons/png/1024x1024.png --prune=true --out=release-builds",
	"package-all" : "npm run package-mac package-win package-linux"
}
...
~~~

Create a packaged application for:

Mac

~~~sh	
> npm run package-mac
~~~ 

Windows

~~~sh	
> npm run package-win
~~~ 

Linux
 
~~~sh	
> npm run package-linux
~~~ 

Build all packages

~~~sh	
> npm run package-all
~~~ 

That's all folks!